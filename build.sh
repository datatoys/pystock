#!/bin/bash

docker-compose pull
docker-compose down
docker-compose build
docker-compose up -d

docker exec pystock_postgres_db_1 bash -c 'until pg_isready; do echo waiting && sleep 5; done;'
sleep 10

rm -r datafiles/*
# run_all.py imports data from yahoo finance into Postgres
docker exec pystock_utilities_1 python utilities/test/run_all.py
docker exec pystock_utilities_1 bash ./utilities/dremio_scripts/setup.sh

docker exec pystock_dbt_1 ./dbt/pystock_dbt.sh

#./dremio_scripts/dremio_postgres_post_dbt_refresh.sh
