import psycopg2
import logging

log = logging.getLogger(__name__)


class PGConnect:

    # Connection parameters
    param_dic = {
        "host": "postgres_db",
        "database": "pystock",
        "user": "root",
        "password": "password",
    }

    def connect(params_dic):
        """ Connect to the PostgreSQL database server """
        conn = None
        try:
            log.debug(
                "connect - connecting to the db {} on {} as {}.".format(
                    params_dic["database"], params_dic["host"], params_dic["user"]
                )
            )
            conn = psycopg2.connect(**params_dic)
        except (Exception, psycopg2.Error) as error:
            log.error(error)
            sys.exit(1)
        log.debug("connect - connected.")
        return conn

    def sql_query(conn, sql):
        """Run a SQL Query"""
        try:
            cur = conn.cursor()

            # execute a statement
            log.debug(sql)
            cur.execute(sql)
            query_out = cur.fetchone()
            log.debug(query_out)

            # close the communication with the PostgreSQL
            cur.close()
        except (Exception, psycopg2.Error) as error:
            log.error(error)
        finally:
            if conn is not None:
                conn.close()
                log.debug("Connection closed.")

    def copy_from_file(conn, datafile, table):
        """
        Here we are going save the dataframe on disk as
        a csv file, load the csv file
        and use copy_from() to copy it to the table
        """
        # Save the dataframe to disk
        # tmp_df = "./tmp_dataframe.csv"
        # df.to_csv(tmp_df, index_label='id', header=False)
        f = open(datafile, "r")
        cursor = conn.cursor()
        try:
            cursor.copy_from(f, table, sep=",")
            conn.commit()
        except Exception as error:
            # os.remove(tmp_df)
            log.error("Error: %s" % error)
            conn.rollback()
            cursor.close()
            return 1
            log.debug("copy_from_file() done")
            cursor.close()
            conn.close()
