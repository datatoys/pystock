import pandas as pd
import pandas.io.sql as sqlio
import yfinance as yf
import pandas_datareader as pdr
from datetime import datetime as dt
from connect import PGConnect as pgc
from psycopg2.extras import Json, DictCursor
import os
import logging

log = logging.getLogger(__name__)


def load_tickers():
    """
    Removes any existing ticker data, then using config file, loads new data to db.
    """
    log.debug("load_tickers: starting")
    ticker_file = "/utilities/config/tickers_multi_col.txt"

    # Proceed if ticker file exists.
    if os.path.exists(ticker_file):

        copy_sql = '''COPY inputs.tickers FROM STDIN with CSV'''
        try:
            conn = pgc.connect(pgc.param_dic)
            cur = conn.cursor()
            # Without autocommit set to True for a proc call, psycopg will go as mad as Norman Bate's mum. But quietly.
            conn.set_session(autocommit=True)
            cur.execute("CALL inputs.delete_tickers()")

            #pgc.copy_from_file(conn, output_file, "inputs.prices")
            with open(ticker_file, 'r') as this_file:       
                cur.copy_expert(copy_sql, this_file)

        except Exception as error:
            log.error(error)
        finally:
            if conn is not None:
                conn.close()

    log.debug("load_tickers - ending")

def get_tickers():
    """
    Returns tickers as list from /config/tickers_single_col.txt.
    """
    ticker_file = "/utilities/config/tickers_single_col.txt"
    tickers = []
    log.debug("get_tickers - ticker_file: {}".format(ticker_file))

    with open(ticker_file, "r") as filehandle:
        tickers = [this_ticker.rstrip() for this_ticker in filehandle.readlines()]
    log.debug("get_tickers - tickers: {}".format(tickers))
    return tickers


def extract_prices():
    """
    Extracts stock price data to file from Yahoo finance for all tickers in
    /config/tickers.txt.

    Checks latest date for ticker in database (public.prices) and pulls from
    Jan 1990 or last full day in db.
    """

    log.debug("extract_prices: starting")
    output_csv = "temp/stock_out.csv"
    output_parquet = "datafiles/stock.parquet"

    if os.path.exists(output_csv):
        os.remove(output_csv)

    # Set the start and end date
    start_date = "1990-01-01"
    end_date = dt.today().strftime("%Y-%m-%d")

    tickers = get_tickers()

    df_all = pd.DataFrame()

    for ticker in tickers:
        conn = pgc.connect(pgc.param_dic)
        cur = conn.cursor()
        cur.execute("SELECT inputs.get_next_price_date(%s)", (ticker,))
        ticker_start_date = cur.fetchone()[0]
        print(f'{ticker: <8}: {'No data' if ticker_start_date is None else ticker_start_date}')

        if not ticker_start_date:
            ticker_start_date = dt.strptime(start_date, "%Y-%m-%d").date()

        if ticker_start_date <= dt.today().date():
            log.debug(
                "extract_prices - calling yfinance for ticker: {}  ticker_start_date: {}".format(
                    ticker, ticker_start_date
                )
            )
            print(f'Pulling {ticker} for {ticker_start_date} to {end_date} from yf')
            # Collect data into inner dataframe df
            yf_ticker = yf.Ticker(ticker)
            df = yf_ticker.history(start=ticker_start_date, end=end_date)
            if not df.empty:
                # Add ticker and retrieved datetime for this stock.
                df["Ticker"] = ticker
                df["Retrieved"] = dt.now().strftime("%Y-%m-%d %H:%M:%S")

                # Reorder frame
                df = df[
                    [
                        "Ticker",
                        "High",
                        "Low",
                        "Open",
                        "Close",
                        "Volume",
                        "Dividends",
                        "Stock Splits",
                        "Retrieved",
                    ]
                ]

                # Append to outer dataframe df_all
                df_all = pd.concat([df_all, df])

    if not df_all.empty:
        # Write to CSV
        log.debug("extract_prices - writing to CSV file: {}".format(output_csv))
        df_all.to_csv(output_csv, header=False)

        df_all["Price Date"] = df_all.index.strftime("%Y-%m-%d")
        # Write to parquet
        log.debug("extract_prices - writing to parquet file: {}".format(output_parquet))
        df_all.to_parquet(
            output_parquet,
            engine="pyarrow",
            index="False",
            partition_cols="Ticker",
            compression="None",
        )
    else:
        log.debug("extract_prices - no prices retrieved")
    log.debug("extract_prices - ending")


def load_prices():
    """
    Removes any partial data, then using file created in extract_stock_prices(),
    loads new data to db.
    """
    log.debug("load_prices: starting")
    output_file = "temp/stock_out.csv"

    # Proceed if any data has been extracted.
    if os.path.exists(output_file):

        copy_sql = '''COPY inputs.prices FROM STDIN with CSV'''
        try:
            conn = pgc.connect(pgc.param_dic)
            cur = conn.cursor()
            # Without autocommit set to True for a proc call, psycopg will go as mad as Norman Bate's mum. But quietly.
            conn.set_session(autocommit=True)
            cur.execute("CALL inputs.delete_partial_prices()")

            #pgc.copy_from_file(conn, output_file, "inputs.prices")
            with open(output_file, 'r') as this_file:       
                cur.copy_expert(copy_sql, this_file)

            # Build week price aggregation
            cur.execute("CALL aggs.build_week_prices()")
            # Build year price aggregation
            cur.execute("CALL aggs.build_year_prices()")
            # Build type 4 slowly changing dimension
            cur.execute("CALL scd.build_prices_history_type_4()")
            # Build type 2 slowly changing dimension
            cur.execute("CALL scd.build_prices_history_type_2()")
        except Exception as error:
            log.error(error)
        finally:
            if conn is not None:
                conn.close()
            os.remove(output_file)

    log.debug("load_prices - ending")


def delete_prices(ticker):
    """
    Deletes stock prices for a single ticker. Used to force a data refresh. If stock is to be permanently
    removed, also remove ticker from /config/tickers/txt.
    """
    log.debug("delete_prices - deleting for {}".format(ticker))
    try:
        conn = pgc.connect(pgc.param_dic)
        # Without autocommit set to True for a proc call, psycopg will go as mad as Norman Bate's mum. But quietly.
        conn.set_session(autocommit=True)
        cur = conn.cursor()

        sql = "CALL inputs.delete_prices(%s);"
        cur.execute(sql, (ticker,))
    except (Exception, pgc.Error) as error:
        log.error(error)
    finally:
        if conn is not None:
            conn.close()
        log.debug("delete_prices - ending")


def extract_statements():
    """
    Extracts financial statement data. More work required :-).
    """
    log.debug("extract_statements - starting")
    tickers = get_tickers()
    for statement_type in ["income_quarterly", "income_annual"]:
        for ticker in tickers:
        # statement_type = "income_quarterly"

            log.debug(
                "extract_statements - calling get_financial_statements for {}/{}".format(
                    ticker, statement_type
                )
            )

            statement_ticker = yf.Ticker(ticker)

            if statement_type == "income_annual":
                statement = statement_ticker.income_stmt
            else:
                statement = statement_ticker.quarterly_income_stmt

            statement = statement.reindex(['Total Revenue', \
                                        'Cost Of Revenue', \
                                        'Gross Profit', \
                                        'Operating Expenses', \
                                        'Operating Income', \
                                        'Pretax Income', \
                                        'Net Income', \
                                        'Basic EPS', \
                                        'Diluted EPS', \
                                        'Basic Average Shares', \
                                        'Diluted Average Shares'],fill_value=0.0)
            
            print(ticker)
            statement = statement.infer_objects().fillna(0.0)
            for period in statement.columns:
                print(period)
                
                total_revenue = statement.at['Total Revenue', period]
                cost_of_revenue = statement.at['Cost Of Revenue', period]
                gross_profit = statement.at['Gross Profit', period]
                operating_expenses = statement.at['Operating Expenses', period]
                operating_income = statement.at['Operating Income', period]
                pretax_income = statement.at['Pretax Income', period]
                net_income = statement.at['Net Income', period]
                basic_eps = statement.at['Basic EPS', period]
                diluted_eps = statement.at['Diluted EPS', period]
                basic_average_shares = statement.at['Basic Average Shares', period]
                diluted_average_shares = statement.at['Diluted Average Shares', period]

                print(total_revenue)
                print(cost_of_revenue)
                print(gross_profit)
                print(operating_expenses)
                print(operating_income)
                print(pretax_income)
                print(net_income)
                print(basic_eps)
                print(diluted_eps)
                print(basic_average_shares)
                print(diluted_average_shares)

                try:
                    conn = pgc.connect(pgc.param_dic)
                    # Without autocommit set to True for a proc call, psycopg will go as mad as Norman Bate's mum. But quietly.
                    conn.set_session(autocommit=True)
                    cur = conn.cursor(cursor_factory=DictCursor)
                    sql = "CALL inputs.insert_statement_income (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
                    cur.execute(
                        sql, (ticker, \
                            period, \
                            statement_type, \
                            total_revenue, \
                            cost_of_revenue, \
                            gross_profit, \
                            operating_expenses, \
                            operating_income, \
                            pretax_income, \
                            net_income, \
                            basic_eps, \
                            diluted_eps, \
                            basic_average_shares, \
                            diluted_average_shares)
                    )
                except Exception as error:
                    print(error)
                finally:
                    if conn is not None:
                        conn.close()
                    log.debug("extract_statements - ending")
