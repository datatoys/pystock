### Ticker Config
- Add one ticker and a carriage return per line
- Ensure tickers are valid for use with Yahoo finance 

#### Example
```
AAF.L
RDSB.L
BP.L
EZJ.L
RTO.L
```
