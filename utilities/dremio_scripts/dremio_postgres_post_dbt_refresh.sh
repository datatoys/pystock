dremio_user="banana"
dremio_pass="bananas4ever"
postgres_user="root"
postgres_pass="password"
postgres_host="192.168.1.125"
postgres_port=5433
postgres_db="pystock"


token=$(curl 'http://192.168.1.125:9047/apiv2/login' -X POST \
     --header 'Content-Type: application/json' \
     --data-raw '{
         "userName":"'"$dremio_user"'",
	 "password":"'"$dremio_pass"'"
     }' \
	     | cat | jq -r '.token') # Passing through cat to force curl to close read stream before passing to jq

token=_dremio$token


curl --location --request POST 'http://192.168.1.125:9047/api/v3/catalog' \
     --header "Authorization: $token" \
     --header 'Content-Type: application/json' \
     --data-raw '{
         "entityType": "source",
	 "name": "Postgres Market Data",
	 "description": "Market data stored in Postgres",
	 "type": "POSTGRES",
	 "config": {"username":"'"$postgres_user"'",
	            "password":"'"$postgres_pass"'",
		    "hostname":"'"$postgres_host"'",
		    "port":"'"$postgres_port"'",
		    "databaseName":"'"$postgres_db"'" 	            
	           }
     }'


curl -X GET 'http://192.168.1.125:9047/api/v3/catalog/by-path/Postgres%20Market%20Data/inputs/prices'
: <<'END'
curl --location --request POST 'http://192.168.1.125:9047/api/v3/catalog' \
     --header "Authorization: $token" \
     --header 'Content-Type: application/json' \
     --data-raw '{
         "entityType": "reflection",
	 "name": "Day Prices - Reflection",
	 "type": "RAW",
	 "config": {"username":"'"$postgres_user"'",
	            "password":"'"$postgres_pass"'",
		    "hostname":"'"$postgres_host"'",
		    "port":"'"$postgres_port"'",
		    "databaseName":"'"$postgres_db"'" 	            
	           }
     }'
END
