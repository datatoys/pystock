this_script="/dremio_scripts/postgres_setup.sh"

# Collect token for subsequent calls
token=$(curl "$DREMIO_BASE_URL/apiv2/login" -X POST \
     --header 'Content-Type: application/json' \
     --data-raw '{
         "userName":"'"$DREMIO_USER"'",
	 "password":"'"$DREMIO_PASS"'"
     }' \
	     | cat | jq -r '.token') # Passing through cat to force curl to close read stream before passing to jq

printf "\n%s: Dremio token collected.\n" $this_script

# Add _dremio prefix to token 
token=_dremio$token

# Add Postgres as a data source
curl --location --request POST "$DREMIO_BASE_URL/api/v3/catalog" \
     --header "Authorization: $token" \
     --header 'Content-Type: application/json' \
     --data-raw '{
         "entityType": "source",
	 "name": "Postgres Market Data",
	 "description": "Market data stored in Postgres",
	 "type": "POSTGRES",
	 "config": {
                 "username":"'"$POSTGRES_USER"'",
	         "password":"'"$POSTGRES_PASS"'",
                 "hostname":"'"$POSTGRES_HOST"'",
                 "port":"'"$POSTGRES_PORT"'",
                 "databaseName":"'"$POSTGRES_DB"'" 	            
                 }
     }'

printf "\n%s: Dremio data source (Postgres Market Data) created.\n" $this_script


curl -X GET "$DREMIO_BASE_URL/api/v3/catalog/by-path/Postgres%20Market%20Data/inputs/prices"
: <<'END'
curl --location --request POST 'http://192.168.1.125:9047/api/v3/catalog' \
     --header "Authorization: $token" \
     --header 'Content-Type: application/json' \
     --data-raw '{
         "entityType": "reflection",
	 "name": "Day Prices - Reflection",
	 "type": "RAW",
	 "config": {"username":"'"$postgres_user"'",
	            "password":"'"$postgres_pass"'",
		    "hostname":"'"$postgres_host"'",
		    "port":"'"$postgres_port"'",
		    "databaseName":"'"$postgres_db"'" 	            
	           }
     }'
END
