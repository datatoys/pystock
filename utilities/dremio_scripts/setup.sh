this_script="/dremio_scripts/setup.sh"
#dremio_base_url="http://192.168.1.123:9047"
#dremio_user="banana"
#dremio_pass="password"
#dremio_email="banana@banana.com"
#postgres_user="root"
#postgres_pass="password"
#postgres_host="192.168.1.123"
#postgres_port=5433
#postgres_db="pystock"

# Create admin user
curl "$DREMIO_BASE_URL/apiv2/bootstrap/firstuser" -X PUT \
      -H 'Authorization: _dremionull' -H 'Content-Type: application/json' \
     --data-binary '{"userName":"'"$DREMIO_USER"'",
                     "firstName":"'"$DREMIO_USER"'",
		     "lastName":"'"$DREMIO_USER"'",
		     "email":"'"$DREMIO_EMAIL"'",
		     "password":"'"$DREMIO_PASS"'"}'

printf "\n%s: Dremio admin user (%s) created.\n" $this_script $dremio_user

# Collect token for subsequent calls
export token=$(curl "$DREMIO_BASE_URL/apiv2/login" -X POST \
     --header 'Content-Type: application/json' \
     --data-raw '{
         "userName":"'"$DREMIO_USER"'",
	 "password":"'"$DREMIO_PASS"'"
     }' \
	     | cat | jq -r '.token') # Passing through cat to force curl to close read stream before passing to jq

printf "\n%s: Dremio token collected.\n" $this_script

# Add _dremio prefix to token 
token=_dremio$token

# Create Markets Space
curl --location --request POST "$DREMIO_BASE_URL/api/v3/catalog" \
     --header "Authorization: $token" \
     --header 'Content-Type: application/json' \
     --data-raw '{
         "entityType": "space",
	 "name": "Markets Space"
     }'

printf "\n%s: Dremio space (Markets Space) created.\n" $this_script


printf "\n%s: Running Postgres setup.\n" $this_script
./utilities/dremio_scripts/postgres_setup.sh

printf "\n%s: Running NAS setup.\n" $this_script
./utilities/dremio_scripts/nas_setup.sh
