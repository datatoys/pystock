## Pystock

### Access
- pgadmin: http://192.168.1.125:8089 (a@b.com/password)
- dremio: http://192.168.1.125:9047 (banana/bananas4ever)
- adminer: http://192.168.1.125:8087
- jupyter: http://192.168.1.125:8009 (docker logs pystock_jupyter_1 2>&1 | grep http)
- python: ```docker exec -it pystock_utilities_1 bash```


### Architecture and Infrastructure
- Postgres - RDBMS
- pgadmin - Postgres IDE
- Adminer - Lightweight database IDE
- Utilities - Python container to handle initial data setup
-- Pull prices from Yahoo Finance per ticker config file
-- Pull statements from Yahoo Finance per ticker config file
-- Create csv and parquet for Prices
-- Create json for Statements
-- Load prices to Postgres
-- Build prices scds in Postgres
-- Build prices week aggregation in Postgres
- DBT - data build tool
-- Build prices week aggregation in Postgres
-- Take prices snapshot
-- Run 1! data test
- Meltano - ELT Framework
-- Not much yet...
- Superset - Reporting
-- Not much yet...
- Great Expectations - Data testing, documentation, profiling
-- Disabled

#### Completed
- Basic data pull
- Ticker file
- Docker-compose
- Add database
- Incremental pull
- Refresh single stock (poss delete single stock, then std import?)

#### To do
- Unit testing - database
- Unit testing - python
- OpenFAAS (or other?)
- Secrets 

### Features
#### Completed
- prices

#### To do
- trendlines
- dividend
- eps
- revenue
- revenue vs eps - is there a gap??
- sector
- market (?)
  - multiple markets?
- sector value (combined prices)
- market value (combined prices)

### Ideas
- Analysis of 2008 data?
- Gold and low risk?

### Resources
- yahoofinancials: https://pypi.org/project/yahoofinancials/

### SCD Tests

```CALL scd.test_scd_by_editing_prices();```

```CALL scd.build_prices_history_type_4();```

```CALL scd.build_prices_history_type_2();```

```SELECT * FROM scd.prices_history_type_4 WHERE ticker = 'AAF.L' 
ORDER BY price_date, retrieved_date;```

```SELECT * FROM scd.prices_history_type_2 WHERE ticker = 'AAF.L' 
ORDER BY price_date, retrieved_date;```

