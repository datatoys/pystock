#!make
include config

ifndef POSTGRES_USER
$(error The POSTGRES_USER variable is missing.)
endif

ifndef POSTGRES_PASSWORD
$(error The POSTGRES_PASSWORD variable is missing.)
endif

ifndef POSTGRES_DB
$(error The POSTGRES_DB variable is missing.)
endif

help:
	@echo ''
	@echo 'Usage: make [TARGET]'
	@echo 'Targets:'
	@echo '  dc_restart            restart all containers'
	@echo '  dc_full_reset         remove all data, rebuild and restart all containers'
	@echo '  dc_up                 start all containers'
	@echo '  dc_down               stop containers'
	@echo '  dc_pull               pull all containers'
	@echo '  dc_build              build all images'
	@echo '  dc_clean              clean all data files'
	@echo '  dc_data_build         run data build'
	@echo '  dc_add_cron           add cron job at midnight for docker-compose pull'
	@echo '  dc_remove_cron        remove cron job for docker-compose pull'

dc_restart: dc_down dc_up

dc_dbt_reset:
dc_full_reset: dc_down dc_remove_database_volume dc_pull dc_build dc_up dc_dbt_reset dc_data_build

dc_up:
	$(info pystock make: docker-compose Starting all containers.)
	docker-compose --env-file ./config -f docker-compose-base.yml up -d

dc_down:
	$(info pystock make: docker-compose Stopping all containers.)
	docker-compose --env-file ./config -f docker-compose-base.yml down
dc_remove_database_volume:
	$(info pystock make: docker-compose removing pg_data volume.)
	docker-compose --env-file ./config -f docker-compose-base.yml down -v
dc_pull:
	$(info pystock make: docker-compose Pulling all containers.)
	docker-compose --env-file ./config -f docker-compose-base.yml pull

dc_build:
	$(info pystock make: docker-compose Building all images.)
	docker-compose --env-file ./config -f docker-compose-base.yml build
dc_clean:
	$(info pystock make: Cleaning up all data files.)
	rm -rf datafiles/*
dc_data_build:
	docker exec pystock_postgres_db_1 bash -c 'until pg_isready; do echo waiting && sleep 5; done;'
	sleep 10
	# run_all.py imports data from yahoo finance into Postgres
	docker exec pystock_utilities_1 python utilities/test/run_all.py
	# docker exec pystock_utilities_1 bash ./utilities/dremio_scripts/setup.sh
	docker exec pystock_dbt_1 ./dbt/pystock_dbt_run.sh
dc_dbt_reset:
	docker exec pystock_dbt_1 ./dbt/pystock_dbt_init.sh

dc_add_cron:
	./crontab_remove.sh
	./crontab_add.sh

dc_remove_cron:
	./crontab_remove.sh

