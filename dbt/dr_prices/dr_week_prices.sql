
{{ config(materialized='table') }}

    WITH week_open AS
    (
        SELECT 
            p.ticker, 
            c.week_number,
            c.year,
            c.week_of_year,	
            p.open_price 
        FROM 
            inputs.prices p
        INNER JOIN 
            utils.calendar c
        ON p.price_date = c.date
        WHERE
            c.day_of_week = 1
    ),
    week_close AS
    (
        SELECT 
            p.ticker, 
            c.week_number, 
            p.close_price 
        FROM 
            inputs.prices p
        INNER JOIN 
            utils.calendar c
        ON p.price_date = c.date
        WHERE
            c.day_of_week = 5
    ),
    week_aggs AS
    (
        SELECT
            p.ticker,
            c.week_number,
            MIN(p.low_price)  AS low_price,
            MAX(p.high_price) AS high_price,
            SUM(p.volume)     AS volume
        FROM 
            inputs.prices p
        INNER JOIN 
            utils.calendar c
        ON p.price_date = c.date
        GROUP BY
            p.ticker,
            c.week_number
    )
    SELECT
        o.ticker,
	o.week_number,
        o.year,
        o.week_of_year,
        a.low_price,
        a.high_price,
        o.open_price,
        c.close_price,
        a.volume
    FROM
        week_open o
    INNER JOIN
        week_close c 
    ON  o.week_number = c.week_number
    AND o.ticker      = c.ticker
    INNER JOIN week_aggs a 
    ON  o.week_number = a.week_number
    AND o.ticker      = a.ticker

