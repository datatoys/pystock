{% snapshot pg_snapshot_prices_check %}

{{
    config(
      target_database='pystock',
      target_schema='dbt',
      unique_key="price_date||'-'||ticker",
      strategy='check',
      check_cols=['high_price', 'low_price', 'open_price', 'close_price', 'volume', 'dividends', 'stock_splits'],
     )
}}
SELECT 
    *
FROM
    {{ source('pg_inputs', 'prices') }}

{% endsnapshot %}
