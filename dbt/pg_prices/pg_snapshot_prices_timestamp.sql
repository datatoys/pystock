{% snapshot pg_snapshot_prices_timestamp %}

{{
    config(
      target_database='pystock',
      target_schema='dbt',
      unique_key="price_date||'-'||ticker",
      strategy='timestamp',
      updated_at='retrieved_date',
     )
}}
SELECT 
    price_date||'-'||ticker AS id,
    *
FROM
    {{ source('pg_inputs', 'prices') }}

{% endsnapshot %}

