#!/bin/bash

dbt init pystock

rm /root/.dbt/profiles.yml
cp /dbt/profiles.yml /root/.dbt/profiles.yml
rm pystock/dbt_project.yml
cp /dbt/dbt_project.yml /pystock/dbt_project.yml

rm -r /pystock/models/example

mkdir /pystock/models/pg_prices
cp /dbt/pg_prices/pg_inputs.yml /pystock/models/pg_prices/pg_inputs.yml
cp /dbt/pg_prices/pg_week_prices.sql /pystock/models/pg_prices/pg_week_prices.sql

mkdir /pystock/snapshots/pg_prices
cp /dbt/pg_prices/pg_snapshot_prices_timestamp.sql /pystock/snapshots/pg_prices/pg_snapshot_prices_timestamp.sql
cp /dbt/pg_prices/pg_snapshot_prices_check.sql /pystock/snapshots/pg_prices/pg_snapshot_prices_check.sql

mkdir /pystock/models/dr_prices
cp /dbt/dr_prices/dr_week_prices.sql /pystock/models/dr_prices/dr_week_prices.sql


