#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER pystock;
    CREATE DATABASE pystock;
    GRANT ALL PRIVILEGES ON DATABASE pystock TO pystock;
    CREATE SCHEMA inputs;
EOSQL

psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
    CREATE SCHEMA inputs;
EOSQL

psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
    CREATE SCHEMA utils;
EOSQL

psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
    CREATE SCHEMA aggs;
EOSQL

psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
    CREATE SCHEMA scd;
EOSQL

psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
    CREATE SCHEMA dbt;
EOSQL

psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
    CREATE SCHEMA simulations;
EOSQL
