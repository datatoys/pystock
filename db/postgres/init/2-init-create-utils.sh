#!/bin/bash
set -e
                   
psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
    SELECT 
        n as num
    INTO utils.numbers
    FROM 
        generate_series(1, 100000) n;

    CREATE UNIQUE INDEX "uci_numbers" ON utils.numbers (num);

    ALTER TABLE utils.numbers CLUSTER ON "uci_numbers";

EOSQL

psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
    WITH cte AS (
        SELECT 
            n + 1 as num,
            CAST('1990-01-01' AS DATE) + (n || ' day')::INTERVAL AS date
        FROM 
            generate_series(0, 20000) n
    )
    SELECT
        date,
        num                     AS day_number,
        CASE WHEN (DATE_PART('dow', date) BETWEEN 1 AND 5)
        THEN RANK() OVER (order by CASE WHEN (DATE_PART('dow', date) BETWEEN 1 AND 5)       
                                   THEN num
                                   ELSE NULL
                                   END)
        END                     AS trading_day_number,
        num/7                   AS week_number,
        DATE_PART('dow', date)  AS day_of_week,
        DATE_PART('week', date) AS week_of_year,
        DATE_PART('year', date) AS year,
		ROW_NUMBER() OVER (PARTITION BY DATE_PART('year', date) ORDER BY date) AS day_of_year,
        CASE WHEN (DATE_PART('dow', date) BETWEEN 1 AND 5)
            THEN true
            ELSE false
        END                     AS is_weekday
    INTO utils.calendar
    FROM 
        cte
    WHERE 
        date < NOW() + INTERVAL '180 day'; 

    CREATE UNIQUE INDEX "uci_calendar" ON utils.calendar (date);

    ALTER TABLE utils.calendar CLUSTER ON "uci_calendar";
EOSQL
