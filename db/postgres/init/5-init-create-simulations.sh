#!/bin/bash
set -e
                   
                   
psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
    CREATE TABLE simulations.metadata 
    (
        sim_id          SERIAL PRIMARY KEY NOT NULL,
        start_dt        TIMESTAMP,
        end_dt          TIMESTAMP,
        sim_result      DECIMAL(30,2),
        sim_metadata    JSON
    );

    CREATE INDEX metadata_sim_id ON simulations.metadata USING BTREE (sim_id);

EOSQL

psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
    CREATE TABLE simulations.detail
    (
        price_date          DATE,
        sim_id              INT,
        ticker              VARCHAR(10), 
        close_price         DECIMAL(30,2),
        ma_price            DECIMAL(30,2),
        bench_ticker        VARCHAR(10),
        bench_close_price   DECIMAL(30,2),
        day_stock_count     INT,
        day_stock_value     DECIMAL(30,2),
        day_cash_value      DECIMAL(30,2),
        day_account_value   DECIMAL(30,2),
        eod_trade_recommend INT,
        day_action          INT,
        volatility_flag     INT
    );

    CREATE INDEX detail_sim_id ON simulations.detail USING BTREE (sim_id);
EOSQL


psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
    CREATE TABLE simulations.trade
    (
        sim_id              INT,
        buy_date            DATE,
        sell_date           DATE,
        ticker              VARCHAR(10),
        units               INT, 
        buy_price           DECIMAL(30,2),
        buy_total           DECIMAL(30,2),
        sell_price          DECIMAL(30,2),
        sell_total          DECIMAL(30,2),
        fees                DECIMAL(30,2),
        price_change        DECIMAL(30,2),
        days_since_trade    INT,
        duration            INT,
        profit              DECIMAL(30,2),
        fire_sale_flag      INT
    );

    CREATE INDEX trade_sim_id ON simulations.trade USING BTREE (sim_id);

EOSQL


psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
    CREATE FUNCTION simulations.sim_results(sim INT) RETURNS TABLE 
    (
        ticker            VARCHAR(10),
        price_date        DATE, 
        close_price       DECIMAL(30,2),
        day_stock_count   INT, 
        day_account_value INT, 
        do_nowt           INT
    )
    LANGUAGE plpgsql
    AS \$\$
    BEGIN
        RETURN QUERY
        WITH s AS (
            SELECT
            d.sim_id, 
            d.close_price                       AS start_close,
            d.day_account_value                 AS start_value,
            CAST(d.day_account_value AS INT) / 
                 CAST(d.close_price AS INT)     AS start_stock,
            d.day_account_value % d.close_price AS start_unspent
        FROM 
            simulations.detail d
        WHERE 
            d.sim_id = sim 
        ORDER BY d.price_date
        LIMIT 1
        )
        SELECT 
             d.ticker,
             d.price_date, 
             d.close_price,
             d.day_stock_count, 
             CAST(d.day_account_value/100 AS INT)                                 AS day_account_value, 
             CAST(((d.close_price * s.start_stock + s.start_unspent)/100) AS INT) AS do_nowt
        FROM 
            simulations.detail d
        INNER JOIN 
            s
        ON  s.sim_id = d.sim_id;
    END;\$\$
EOSQL


psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL

    CREATE FUNCTION simulations.summary() RETURNS TABLE 
    (
        sim_id                  INT, 
        sim_result          DECIMAL(30,2), 
        ticker                  VARCHAR(10),
        moving_avg_days INT,
        buy_threshold   DECIMAL(3,2),
        sell_threshold  DECIMAL(3,2)
    )
    LANGUAGE plpgsql
    AS \$\$
    BEGIN
        RETURN QUERY
        SELECT 
            m.sim_id, 
            m.sim_result, 
            CAST(m.sim_metadata ->> 'ticker' AS VARCHAR(10))         AS ticker,
            CAST(m.sim_metadata ->> 'moving_avg_days'AS INT)         AS moving_avg_days,
            CAST(m.sim_metadata ->> 'buy_threshold' AS DECIMAL(4,3)) AS buy_threshold,
            CAST(m.sim_metadata ->> 'sell_threshold'AS DECIMAL(4,3)) AS sell_threshold
        FROM 
            simulations.metadata m
        ORDER BY 
            m.sim_result DESC;
    END; \$\$ 
EOSQL


