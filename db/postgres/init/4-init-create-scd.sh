#!/bin/bash
set -e
                   

psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
    CREATE TABLE scd.prices_history_type_2 
    (
        price_date     DATE,
        ticker         VARCHAR(10), 
        high_price     DECIMAL(30,15), 
        low_price      DECIMAL(30,15), 
        open_price     DECIMAL(30,15), 
        close_price    DECIMAL(30,15),
        volume         DECIMAL(30,15), 
        dividends      DECIMAL(30,15), 
        stock_splits   DECIMAL(30,15), 
        retrieved_date TIMESTAMP,
        valid_to_date  TIMESTAMP
    );
EOSQL


psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
    CREATE TABLE scd.prices_history_type_4 
    (
        price_date     DATE,
        ticker         VARCHAR(10), 
        high_price     DECIMAL(30,15), 
        low_price      DECIMAL(30,15), 
        open_price     DECIMAL(30,15), 
        close_price    DECIMAL(30,15),
        volume         DECIMAL(30,15), 
        dividends      DECIMAL(30,15), 
        stock_splits   DECIMAL(30,15), 
        retrieved_date TIMESTAMP
    );
EOSQL



psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
CREATE OR REPLACE PROCEDURE scd.build_prices_history_type_4()
LANGUAGE plpgsql    
AS \$\$
--------------------------------------------------------------------------------
-- Builds type 4 scd for prices
--------------------------------------------------------------------------------
BEGIN

    INSERT INTO scd.prices_history_type_4
    (
        price_date,
        ticker,
        low_price,
        high_price,
        open_price,
        close_price,
        volume,
        dividends,
        stock_splits,
        retrieved_date
    )
    SELECT
        p.price_date,
        p.ticker,
        p.low_price,
        p.high_price,
        p.open_price,
        p.close_price,
        p.volume,
        p.dividends,
        p.stock_splits,
        p.retrieved_date
    FROM
        inputs.prices p
    LEFT JOIN
	scd.prices_history_type_4 h
    ON  p.price_date     = h.price_date
    AND p.ticker         = h.ticker
    AND p.low_price      = h.low_price
    AND p.high_price     = h.high_price
    AND p.open_price     = h.open_price
    AND p.close_price    = h.close_price
    AND p.volume         = h.volume
    AND p.dividends      = h.dividends
    AND p.stock_splits   = h.stock_splits
    AND p.retrieved_date = h.retrieved_date
    WHERE 
        h.ticker IS NULL;

END;\$\$
EOSQL


psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
CREATE OR REPLACE PROCEDURE scd.build_prices_history_type_2()
LANGUAGE plpgsql    
AS \$\$
--------------------------------------------------------------------------------
-- Builds type 2 scd for prices
--------------------------------------------------------------------------------
BEGIN
    TRUNCATE TABLE scd.prices_history_type_2;

    INSERT INTO scd.prices_history_type_2
    (
        price_date,
        ticker,
        low_price,
        high_price,
        open_price,
        close_price,
        volume,
        dividends,
        stock_splits,
        retrieved_date,
        valid_to_date
    )
    SELECT
        price_date,
        ticker,
        low_price,
        high_price,
        open_price,
        close_price,
        volume,
        dividends,
        stock_splits,
        retrieved_date,
        LEAD(retrieved_date, 1, NULL) OVER (PARTITION BY price_date, ticker ORDER BY retrieved_date)
    FROM
	scd.prices_history_type_4;

END;\$\$

EOSQL
psql -v ON_ERROR_STOP=1 --username "pystock" --dbname "pystock" <<-EOSQL
CREATE OR REPLACE PROCEDURE scd.test_scd_by_editing_prices()
LANGUAGE plpgsql    
AS \$\$
--------------------------------------------------------------------------------
-- Alter some price data to test slowly changing dimensions
--------------------------------------------------------------------------------
BEGIN

    UPDATE inputs.prices
    SET high_price = -1, retrieved_date = CURRENT_TIMESTAMP
    WHERE 
        ticker = 'AAF.L';
  
    DELETE FROM inputs.prices
    WHERE ticker = 'BP.L';

END;\$\$
EOSQL
